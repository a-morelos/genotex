# Genotex

**An Elixir implementation of [Genote](https://github.com/a-morelos/genote)**

It's a script that create 2 files that contains HTML tags for references and notes. Requires Elixir to be install for use.

## Usage

`genotex [OPTIONS]`

## Options

```
--num, -n       Number of notes you will create
--src, -s       Filename for original reference
--dest, -d      Filename of destination notes
--help, -h      Show this help
```

### Range options

You can specify a set of notes with a range

```
--from, -f      Define start of range
--to, -t        Define end of range. Standalone option works as same as --num
```

## Examples

Create 5 notes

`genotex --num 5`

Create notes with id 15 to 23

`genotex --from 15 --to 23`

Create 3 notes which will comes from file introduction.xhtml

`genotex --num 3 --src introduction`

## Build

Just clone this repo and use `mix escript.build`