defmodule GenotexTest do
  use ExUnit.Case
  doctest Genotex

  @doc """
  Tests for parse arguments
  """
  describe "Parameters" do
    test "One number indeed options" do
      assert Genotex.parse_args(["--num", "13"]) == {[num: 13], []}
    end

    test "Range options" do
      assert Genotex.parse_args(["--from", "14", "--to", "20"]) == {[from: 14, to: 20], []}
    end

    test "src options" do
      assert Genotex.parse_args(["--num", "14", "--src", "cap04"]) ==
               {[num: 14, src: "cap04"], []}
    end
  end
end
