defmodule Genotex do
  @moduledoc false

  @doc """
  Main function. Get arguments to use.

  """
  def main(argsv \\ []) do
    {opts, _value} = parse_args(argsv)

    if Keyword.has_key?(opts, :help) do
      show_help()
    end

    # create a list for notes index
    list_notes = create_range(opts)
    # create a map with file name references.
    files = define_files(opts)

    generate_files(list_notes, files)
  end

  @doc """
  Parse arguments from command line useing OptionParser module

  Examples

     iex> Genotex.parse_args(["--num", "5", "--src", "cap01"])
     {[num: 5, src: "cap01"], []}
  """
  def parse_args(argsv) do
    options = [
      aliases: [n: :num, f: :from, t: :to, s: :src, d: :dest, h: :help],
      switches: [num: :integer,
                from: :integer,
                to: :integer,
                src: :string,
                dest: :string]
    ]

    {opts, value, _} =
      argsv
      |> OptionParser.parse(options)

    {opts, value}
  end

  @doc """
  Create an html tag for use as note.

  ## Parameters
  - id: An integer for use as indentifier
  - src: String used as filename origin
  """
  def create_note(id, src) do
    note = """
      <div class=\"nota\">\n\t<p id=\"nt#{id}\"\><sup>[#{id}]</sup> **Text note here** <a href=\"../Text/#{src}#rf#{id}\">&lt;&lt;</a></p>
      </div>\n\n
      """
    note
  end

  @doc """
  Create an html tag for use as reference.

  ## Parameters
  - id: An integer for use as indentifier
  - dest: String used as filename destiny
  """
  def create_ref(id, dest) do
    ref = "<a href=\"../Text/#{dest}\#nt#{id}\" id=\"rf#{id}\"><sup>[#{id}]</sup></a>\n"
    ref
  end

  @doc """
  Create a lists from parameters asociated with range

  ## Parameters

  - options. An keyword with parameters for define range

  Examples

  iex> opts = [num: 3]
  iex> Genotex.create_range(opts)
  [1, 2, 3]

  iex> opts = [from: 15, to: 20]
  iex> Genotex.create_range(opts)
  [15, 16, 17, 18, 19, 20]
  """
  def create_range(options) do
    cond do
      Keyword.has_key?(options, :num) ->
        Enum.map(1..options[:num], fn x -> x end)

      Keyword.has_key?(options, :from) && Keyword.has_key?(options, :to) ->
        Enum.map(options[:from]..options[:to], fn x -> x end)

      Keyword.has_key?(options, :from) ->
        Enum.map(options[:from]..(options[:from] + 1), fn x -> x end)

      Keyword.has_key?(options, :to) ->
        Enum.map(0..options[:to], fn x -> x end)

      true ->
        [1]
    end
  end

  @doc """

  Return a map with source and destiny file names

  ## Parameters

  - options. It's a list with options from `parse_args()`

  Examples

  iex>opts = [src: "chapter01", dest: "notes"]
  iex> Genotex.define_files(opts)
  %{:dest => "notes.xhtml", :src => "chapter01.xhtml"}

  iex>opts = [src: "chapter05"]
  iex> Genotex.define_files(opts)
  %{:dest => "notes.xhtml", :src => "chapter05.xhtml"}

  iex>opts = [dest: "notes"]
  iex> Genotex.define_files(opts)
  %{:dest => "notes.xhtml", :src => "chapter01.xhtml"}

  iex>opts = [num: 4]
  iex> Genotex.define_files(opts)
  %{:dest => "notes.xhtml", :src => "chapter01.xhtml"}

  """
  def define_files(options) do
    cond do
      Keyword.has_key?(options, :src) && Keyword.has_key?(options, :dest) ->
        %{:src => options[:src] <> ".xhtml", :dest => options[:dest] <> ".xhtml"}

      Keyword.has_key?(options, :src) ->
        %{:src => options[:src] <> ".xhtml", :dest => "notes.xhtml"}

      Keyword.has_key?(options, :dest) ->
        %{:src => "chapter01.xhtml", :dest => options[:dest] <> ".xhtml"}

      true ->
        %{:src => "chapter01.xhtml", :dest => "notes.xhtml"}
    end
  end

  def generate_files(list_notes, files) do
    # Define filenames for output files
    filename_refs = "references_" <> files[:src]
    filename_notes = "notes_" <> files[:src]

    # Create output files for notes and references
    {ok, file_notes} = File.open(Path.relative("#{filename_notes}"), [:write, :read, :append])
    {ok, file_refs} = File.open(Path.relative("#{filename_refs}"), [:write, :read, :append])

    for x <- list_notes do
      # IO.pu(Path.relative("#{notes}"), create_note(x, files[:src]))
      IO.write(file_notes, create_note(x, files[:src]))
      IO.write(file_refs, create_ref(x, files[:dest]))
    end

    # Close files before exit
    File.close(file_notes)
    File.close(file_refs)

    {:ok, filename_notes, filename_refs}
  end

  def show_help do
    help_text = """
    Genotex it's a script that create 2 files that contains HTML tags for references and notes. Requires Elixir to be install for use.

    # Usage

    $ genotex [OPTIONS]

    # Options

    --num, -n       Number of notes you will create
    --src, -s       Filename for original reference
    --dest, -d      Filename of destination notes
    --help, -h      Show this help

    ## Range options

    You can specify a set of notes with a range

    --from, -f      Define start of range
    --to, -t        Define end of range. Standalone option works as same as --num
    """

    IO.puts(help_text)
    System.halt(0)
  end
end
